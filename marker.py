# Marked selected text
#
# Example:
# cat apache2.conf | python marker.py apache Server ip =
#
# By Flopik  
# Latest change 30.07.2015 3:58
import sys
import re
import random

data = sys.stdin.read()
marked = sys.argv[1:]

def mixColor(colorLenght):
	temp = []
	for id in range(colorLenght):
		temp.append(random.randint(44,46))

	return temp

colorTable = mixColor(len(marked))

def applyReplace(id, source, matched):
	return re.sub("(%s)" % matched, "\33[1;%im\\1\033[1;m" % colorTable[id], source)

for id, line in enumerate(marked):
	data = applyReplace(id, data, line)

print(data)
