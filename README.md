# README #

### For? ###
..highlight text in log, config, etc

### Example use ###
cat apache2.conf | python marker.py apache Server ip =

cat user_manuals.txt | python marker.py "http://\w+\.net\/"

### Alias ###
alias marker='python DIR/marker.py'

cat apache2.conf | marker apache Server ip =

cat user_manuals.txt | marker "http://\w+\.net\/"